# kafka-twitter

## :warning: 1 - Add dependencies

Add the following dependencies to the `pom.xml` file for twitter producer:

```xml
    <dependencies>
        <!-- https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients -->
        <dependency>
            <groupId>org.apache.kafka</groupId>
            <artifactId>kafka-clients</artifactId>
            <version>2.0.0</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-simple -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.25</version>
        </dependency>

        <!-- https://github.com/twitter/hbc -->
        <dependency>
            <groupId>com.twitter</groupId>
            <artifactId>hbc-core</artifactId> <!-- or hbc-twitter4j -->
            <version>2.2.0</version> <!-- or whatever the latest version is -->
        </dependency>
    </dependencies>

```

For Elasticsearch consumer

```xml
    <dependencies>
        <!-- https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients -->
        <dependency>
            <groupId>org.apache.kafka</groupId>
            <artifactId>kafka-clients</artifactId>
            <version>2.0.0</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-simple -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.25</version>
        </dependency>

        <dependency>
            <groupId>org.elasticsearch.client</groupId>
            <artifactId>elasticsearch-rest-high-level-client</artifactId>
            <version>7.9.0</version>
        </dependency>
        
        <!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.5</version>
        </dependency>

</dependencies>
```

## :rocket: 2 - Quickstart

Follow the quickstart information for the [streaming twitter api](https://github.com/twitter/hbc)

To keep your secret keys safe, and avoid to share them on this repository, create a java class
called `ApiConfig` at `kafka-producer-twitter` module, to store your Twitter Credentials

```java
public class ApiConfig {

    private String consumerKey = "XXXXXXXX";
    private String consumerSecret = "XXXXXXXX";
    private String token = "XXXXXXXX";
    private String secret = "XXXXXXXX";

    public String getConsumerKey() {
        return consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public String getToken() {
        return token;
    }

    public String getSecret() {
        return secret;
    }
}
```

Then create another file to store your ElasticSearch credentials at `kafka-elasticsearch-consumer` module
called ESConfig:

```java
public class ESConfig {

    private String hostname = "XXXXXXXXXXXX";
    private String username = "XXXXXXXXXXXX";
    private String password = "XXXXXXXXXXXX";

    public String getHostname() {
        return hostname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
```
## :penguin: 3 - Tips

When searching on ElasticSearch, if you have a version above 7, remember that _docs
are no longer named, so, to search in the console you have to:

```
    /index/_doc/{id}
```